import {javascriptConfig} from "./flat/javascript.js"
import {jsDocConfig} from "./flat/jsdoc.js"

export default [javascriptConfig, jsDocConfig]
