import {reactTypescriptConfig} from "./flat/react-typescript.js"
import typescriptRecommended from "./typescript-recommended.js"

export default [...typescriptRecommended, reactTypescriptConfig]
