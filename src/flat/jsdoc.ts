import {Linter} from "eslint"
import jsDocESLint from "eslint-plugin-jsdoc"

import {javascriptConfig} from "./javascript.js"

export const jsDocConfig = {
  name: "99protsenti/jsdoc",
  files: javascriptConfig.files,
  plugins: {jsdoc: jsDocESLint},
  rules: {
    ...jsDocESLint.configs.recommended.rules,
    "jsdoc/newline-after-description": "off",
    "jsdoc/require-jsdoc": [
      "error",
      {
        exemptEmptyFunctions: true,
        exemptEmptyConstructors: true,
        enableFixer: false,
        require: {
          FunctionDeclaration: true,
          MethodDefinition: true,
        },
      },
    ],
  },
} satisfies Linter.Config
