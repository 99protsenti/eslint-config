import type {FlatConfig as TSFlatConfig} from "@typescript-eslint/utils/ts-eslint"

import {jsDocConfig} from "./jsdoc.js"
import {typescriptConfig} from "./typescript.js"

export const typescriptJsDocConfig = {
  ...jsDocConfig,
  files: typescriptConfig.files,
  languageOptions: typescriptConfig.languageOptions,
  rules: {
    ...jsDocConfig.rules,
    "jsdoc/require-param-type": "off",
    "jsdoc/require-returns-type": "off",
    "jsdoc/no-types": "error",
  },
} satisfies TSFlatConfig.Config
