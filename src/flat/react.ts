import {ESLint, Linter} from "eslint"
import reactESLint from "eslint-plugin-react"

export const reactConfig = {
  name: "99protsenti/react",
  files: ["**/*.jsx"],
  languageOptions: reactESLint.configs.flat!.recommended.languageOptions,
  plugins: {react: reactESLint as ESLint.Plugin},
  rules: {
    ...reactESLint.configs.flat!.recommended.rules,
    "no-console": ["warn", {allow: ["info", "warn", "error", "assert"]}],
  },
} satisfies Linter.Config
