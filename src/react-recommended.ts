import {reactConfig} from "./flat/react.js"
import javascriptRecommended from "./javascript-recommended.js"

export default [...javascriptRecommended, reactConfig]
