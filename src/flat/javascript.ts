import javascriptESLint from "@eslint/js"
import {Linter} from "eslint"

export const javascriptConfig = {
  name: "99protsenti/javascript",
  files: ["**/*.js", "**/*.mjs", "**/*.cjs", "**/*.jsx"],
  rules: {
    ...javascriptESLint.configs.recommended.rules,
    "no-console": "off",
    "no-alert": "error",
    "eqeqeq": "error",
    "no-useless-computed-key": "warn",
    "no-debugger": "warn",
    "no-extra-parens": "warn",
    "no-negated-in-lhs": "error",
    "no-shadow": "error",
    "no-label-var": "error",
    "no-undef-init": "error",
    "no-undefined": "error",
    "no-use-before-define": "error",
    "no-constant-binary-expression": "error",
    "no-implicit-coercion": "error",
    "prefer-object-has-own": "error",
    "require-atomic-updates": "error",
    "object-shorthand": "error",
    "no-sequences": "error",
    "no-lonely-if": "error",
  },
} satisfies Linter.Config
