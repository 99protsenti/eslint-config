## [2.1.0](https://gitlab.com/99protsenti/eslint-config/compare/v2.0.2...v2.1.0) (2025-02-25)

### Features

* **typescript:** enforce typescript recommended type checked configuration ([d206640](https://gitlab.com/99protsenti/eslint-config/commit/d206640f6cf0b7af4bd1b36e378ec4f40eeb301d))

## [2.0.2](https://gitlab.com/99protsenti/eslint-config/compare/v2.0.1...v2.0.2) (2025-02-25)

### Bug Fixes

* **typescript:** enforce typescript recommended configuration ([7a7a890](https://gitlab.com/99protsenti/eslint-config/commit/7a7a89005e8c6d87caf81daec413c39d6177e405))

## [2.0.1](https://gitlab.com/99protsenti/eslint-config/compare/v2.0.0...v2.0.1) (2025-02-25)

### Bug Fixes

* **typescript:** fix typescript configuration file glob ([d96f0f4](https://gitlab.com/99protsenti/eslint-config/commit/d96f0f4844ef84b980f2a3f31727170c72243bb0))

## [2.0.0](https://gitlab.com/99protsenti/eslint-config/compare/v1.0.0...v2.0.0) (2024-12-08)

### ⚠ BREAKING CHANGES

* migrated to the flat eslint configuration

### Features

* upgrade eslint and migrate to the flat config ([954e4b9](https://gitlab.com/99protsenti/eslint-config/commit/954e4b9851c68a0946460f1109b66a7776d9b2bc))

### Bug Fixes

* **bundle:** remove legacy dependencies ([f7e5d5d](https://gitlab.com/99protsenti/eslint-config/commit/f7e5d5dec0b9ca064a0939327c4b9d204006d0ef))

## 1.0.0 (2023-10-14)


### Features

* create basic eslint configuration set ([518bbfd](https://gitlab.com/99protsenti/eslint-config/commit/518bbfd005d2aeef03ece4c9d9c640bcd2b57bdd))
