import type {FlatConfig as TSFlatConfig} from "@typescript-eslint/utils/ts-eslint"

import {reactConfig} from "./react.js"
import {typescriptConfig} from "./typescript.js"

export const reactTypescriptConfig = {
  ...reactConfig,
  name: "99protsenti/react-typescript",
  files: [...reactConfig.files, "**/*.tsx"],
  languageOptions: {
    ...reactConfig.languageOptions,
    ...typescriptConfig.languageOptions,
  },
  plugins: {
    ...reactConfig.plugins,
    ...typescriptConfig.plugins,
  },
} satisfies TSFlatConfig.Config
