import {javascriptConfig} from "./flat/javascript.js"
import {typescriptJsDocConfig} from "./flat/jsdoc-typescript.js"
import {typescriptConfig} from "./flat/typescript.js"

export default [javascriptConfig, typescriptConfig, typescriptJsDocConfig]
