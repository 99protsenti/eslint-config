import javascriptESLint from "@eslint/js"
import getTypescriptESLintBaseConfig from "@typescript-eslint/eslint-plugin/use-at-your-own-risk/eslint-recommended-raw"
import type {FlatConfig as TSFlatConfig} from "@typescript-eslint/utils/ts-eslint"
import typescriptESLint from "typescript-eslint"

const typescriptBaseConfig = getTypescriptESLintBaseConfig("glob")
const typescriptRecommendedRules =
  typescriptESLint.configs.recommendedTypeChecked.reduce(
    (accumulator, currentValue) =>
      Object.assign({}, accumulator, currentValue.rules),
    {} as TSFlatConfig.Config["rules"],
  )
export const typescriptConfig = {
  name: "99protsenti/typescript",
  files: typescriptBaseConfig.files.map(glob => `**/${glob}`),
  languageOptions: {
    parser: typescriptESLint.parser,
    parserOptions: {
      projectService: true,
      tsconfigRootDir: import.meta.dirname,
    },
    sourceType: "module",
  },
  plugins: {"@typescript-eslint": typescriptESLint.plugin},
  rules: {
    ...javascriptESLint.configs.recommended.rules,
    ...typescriptBaseConfig.rules,
    ...typescriptRecommendedRules,
    "@typescript-eslint/no-non-null-assertion": "error",
    "@typescript-eslint/no-shadow": "error",
    "@typescript-eslint/no-unsafe-unary-minus": "error",
    "@typescript-eslint/use-unknown-in-catch-callback-variable": "warn",
  },
} satisfies TSFlatConfig.Config
